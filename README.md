MovieFinder

Sample Movie app using RxJava3, View Binding, Jetpack Components( View Model, Navigation Component, Room, Live Data),and Retrofit.
![Sheme](https://bitbucket.org/cl088803010/moviefinder/src/master/home.jpg?raw=true)
MovieFinder is built on MVVM architecture.

Libraries Used:
RxJava3
ViewModel
LiveData
ViewBinding
Retrofit
Room
Navigation Component


API used:
TMDB
