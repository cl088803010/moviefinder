package com.lc.moviefinder.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.os.bundleOf
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.lc.moviefinder.Constants
import com.lc.moviefinder.R
import com.lc.moviefinder.httpRequest.model.Movie
import javax.inject.Inject

class HomeRVAdapter @Inject constructor(var movies: ArrayList<Movie>): RecyclerView.Adapter<HomeRVAdapter.ViewHolder>() {
    class ViewHolder(itemView: View, val context: Context) : RecyclerView.ViewHolder(itemView) {

        val movieLayout: ConstraintLayout = itemView.findViewById(R.id.movieLayout)
        fun bind(movies: List<Movie>, position: Int){
            movieLayout.clipToOutline = true
            movieLayout.setOnClickListener {
                val bundle = bundleOf(MOVIE_ID_KEY to movies.get(position).id)
                Navigation.findNavController(it).navigate(R.id.action_homeFragment_to_movieDetailFragment, bundle)
            }

            itemView.findViewById<TextView>(R.id.movieLikeTv).setText((movies.get(position).voteCount).toString())
            itemView.findViewById<TextView>(R.id.movieNameTv).setText(movies.get(position).title)
            Glide.with(context)
                .load(Constants.IMAGE_BASE_URL + movies.get(position).posterPath)
                .into(itemView.findViewById(R.id.movieIv))
        }

        companion object{
            const val MOVIE_ID_KEY = "movieID"
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.movie_item_layout, parent,false)

        return ViewHolder(view, parent.context)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(movies, position)
    }

    override fun getItemCount(): Int {
        return movies.size
    }

    fun addMovies(movies: ArrayList<Movie>){
        this.movies = movies
        this.notifyItemInserted(0)
    }
}