package com.lc.moviefinder.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.os.bundleOf
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.lc.moviefinder.Constants
import com.lc.moviefinder.R
import com.lc.moviefinder.httpRequest.model.Movie
import javax.inject.Inject

class HomeViewPagerAdapter(var movies: ArrayList<Movie>):
    RecyclerView.Adapter<HomeViewPagerAdapter.ViewHolder>() {

    class ViewHolder(itemView: View, val context: Context) : RecyclerView.ViewHolder(itemView) {
        val curShowingLayout: ConstraintLayout = itemView.findViewById(R.id.curShowingItemLayout)

        fun bind(movies: ArrayList<Movie>, position: Int){
            curShowingLayout.clipToOutline = true
            curShowingLayout.setOnClickListener {
                val bundle = bundleOf(HomeRVAdapter.ViewHolder.MOVIE_ID_KEY to movies.get(position).id)
                Navigation.findNavController(it).navigate(R.id.action_homeFragment_to_movieDetailFragment, bundle)
            }
            Glide.with(context)
                .load(Constants.IMAGE_BASE_URL + movies.get(position).backdropPath)
                .into(itemView.findViewById(R.id.curShowingIv))
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
                                 .inflate(R.layout.currently_showing_item_layout, parent, false)
        return ViewHolder(view, parent.context)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(movies, position)
    }

    override fun getItemCount(): Int {
        return movies.size
    }

    fun addMovies(movies: ArrayList<Movie>){
        this.movies = movies
        this.notifyDataSetChanged()
    }
}