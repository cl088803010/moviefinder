package com.lc.moviefinder.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RatingBar
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.os.bundleOf
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.lc.moviefinder.Constants
import com.lc.moviefinder.R
import com.lc.moviefinder.httpRequest.model.Movie
import javax.inject.Inject
import javax.inject.Singleton

class MoviesRVAdapter(var movies: ArrayList<Movie>): RecyclerView.Adapter<MoviesRVAdapter.ViewHolder>() {

    class ViewHolder(itemView: View, val context: Context) : RecyclerView.ViewHolder(itemView) {

        val yearTv = itemView.findViewById<TextView>(R.id.movieYearTv)
        val movieIv = itemView.findViewById<ImageView>(R.id.movieIv)
        val movieNameTv = itemView.findViewById<TextView>(R.id.movieNameTv)
        val ratingBar = itemView.findViewById<RatingBar>(R.id.ratingBar)
        val movieGenreTv = itemView.findViewById<TextView>(R.id.movieGenreTv)
        val movieLayout = itemView.findViewById<ConstraintLayout>(R.id.movieLayout)

        fun bind(movies: ArrayList<Movie>, position: Int){

            movieLayout.clipToOutline = true
            //update date
            val dateArray = movies.get(position).releaseDate.split("-")
            yearTv.setText(dateArray[0])
            //update image
            Glide.with(context).load(Constants.IMAGE_BASE_URL + movies.get(position).posterPath).into(movieIv)
            //update title
            movieNameTv.setText(movies.get(position).title)
            //update genre
            val sb = StringBuilder()
            val movieGenreIDs = movies.get(position).genreIds
            movieGenreIDs.forEach {
                sb.append(Constants.getGenreMap()?.get(it)).append("|")
            }
            sb.deleteCharAt(sb.length - 1)
            movieGenreTv.setText(sb.toString())
            //update rating
            ratingBar.rating= movies.get(position).voteAverage.toFloat() / 2

            movieLayout.setOnClickListener {
                val bundle = bundleOf(HomeRVAdapter.ViewHolder.MOVIE_ID_KEY to movies.get(position).id)
                Navigation.findNavController(it).navigate(R.id.action_moviesFragment_to_movieDetailFragment, bundle)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.view_all_movie_item_layout, parent, false)
        return ViewHolder(view, parent.context)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(movies, position)
    }

    override fun getItemCount(): Int {
        return movies.size
    }

    fun addMovies(movies: ArrayList<Movie>){
        this.movies = movies
        notifyDataSetChanged()
    }
}