package com.lc.moviefinder.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.lc.moviefinder.Constants
import com.lc.moviefinder.R
import com.lc.moviefinder.httpRequest.model.Cast

class CastsRVAdapter(var casts: ArrayList<Cast>) : RecyclerView.Adapter<CastsRVAdapter.ViewHolder>() {
    class ViewHolder(itemView: View, val context: Context): RecyclerView.ViewHolder(itemView){
        fun bind(casts: ArrayList<Cast>, position: Int){
            Glide.with(context)
                .load(Constants.IMAGE_BASE_URL + casts.get(position).profilePath)
                .apply(RequestOptions.circleCropTransform())
                .into(itemView.findViewById<ImageView>(R.id.castIv))

            itemView.findViewById<TextView>(R.id.actorNameTv).setText(casts.get(position).name)
            itemView.findViewById<TextView>(R.id.charNameTv).setText(casts.get(position).character)

        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.cast_item_layout, parent, false)
        return ViewHolder(view, parent.context)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(casts, position)
    }

    override fun getItemCount(): Int {
        return casts.size
    }

    fun addCasts(casts: ArrayList<Cast>){
        this.casts = casts
        notifyDataSetChanged()
    }
}