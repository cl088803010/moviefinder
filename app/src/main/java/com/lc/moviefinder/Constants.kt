package com.lc.moviefinder

import androidx.annotation.Keep
import java.util.*

object Constants {

    const val IMAGE_BASE_URL = "https://image.tmdb.org/t/p/w500"

    enum class MovieCategory{UPCOMING, POPULAR, CURRENTLY_SHOWING, TOP_RATED}

    fun getGenreMap(): HashMap<Int, String>? {
        val genreMap = HashMap<Int, String>()
        genreMap[28] = "Action"
        genreMap[12] = "Adventure"
        genreMap[16] = "Animation"
        genreMap[35] = "Comedy"
        genreMap[80] = "Crime"
        genreMap[99] = "Documentary"
        genreMap[18] = "Drama"
        genreMap[10751] = "Family"
        genreMap[14] = "Fantasy"
        genreMap[36] = "History"
        genreMap[27] = "Horror"
        genreMap[10402] = "Music"
        genreMap[9648] = "Mystery"
        genreMap[10749] = "Romance"
        genreMap[878] = "Science Fiction"
        genreMap[53] = "Thriller"
        genreMap[10752] = "War"
        genreMap[37] = "Western"
        genreMap[10770] = "TV Movie"
        return genreMap
    }
}