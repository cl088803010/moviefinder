package com.lc.moviefinder.di

import android.content.Context
import com.lc.moviefinder.BaseApplication
import com.lc.moviefinder.httpRequest.model.Movie
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton
@Module
@InstallIn(SingletonComponent::class)
object AppModule {
    @Singleton
    @Provides
    fun providesApplication(@ApplicationContext app: Context): BaseApplication{
        return app as BaseApplication
    }

    @Singleton
    @Provides
    fun providesHelloWorldStr(): String{
        return "Hello world"
    }

    @Singleton
    @Provides
    fun providesMovies(): ArrayList<Movie>{
        return ArrayList()
    }
}