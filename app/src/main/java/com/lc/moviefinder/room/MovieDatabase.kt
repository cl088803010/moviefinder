package com.lc.moviefinder.room

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

@Database(entities = arrayOf(WishListMovie::class), version = 1, exportSchema = false)
abstract class MovieDatabase: RoomDatabase() {
    abstract fun WishListMovieDao(): WishListMovieDao

    companion object{
        @Volatile
        var mInstance: MovieDatabase? = null

        fun getInstance(context: Context): MovieDatabase{
            return mInstance ?: synchronized(this){
                val instance = Room.databaseBuilder(context.applicationContext, MovieDatabase::class.java, "movie_database").build()
                mInstance = instance
                instance
            }
        }
    }
}