package com.lc.moviefinder.room

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import com.lc.moviefinder.httpRequest.model.Movie
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.Flowable
import io.reactivex.rxjava3.core.Single
import kotlinx.coroutines.flow.Flow

@Dao
interface WishListMovieDao {
    @Insert
    fun insertWishListMovie(movie: WishListMovie): Completable

    @Query("SELECT * FROM wishlist_movie")
    fun getWishListMovies(): Flowable<List<WishListMovie>>

    @Query("SELECT * FROM wishlist_movie WHERE id = :movieID")
    fun getWishListMovieByID(movieID: Int): Flowable<WishListMovie>

    @Query("DELETE FROM wishlist_movie")
    fun clearWishLisMovies(): Completable

    @Query("DELETE FROM wishlist_movie WHERE id = :movieID")
    fun deleteWishListMovieByID(movieID: Int): Completable
}