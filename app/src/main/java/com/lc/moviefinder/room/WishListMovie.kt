package com.lc.moviefinder.room

import androidx.annotation.NonNull
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters

@Entity(tableName = "wishlist_movie")
data class WishListMovie(val adult: Boolean,
                         @ColumnInfo(name = "backdrop_path") val backdropPath: String,
                         @PrimaryKey(autoGenerate = true) @NonNull val id: Int,
                         @ColumnInfo(name = "original_language") val originalLanguage: String,
                         @ColumnInfo(name = "original_title") val originalTitle: String,
                         val overview: String,
                         val popularity: Double,
                         @ColumnInfo(name = "poster_path") val posterPath: String,
                         @ColumnInfo(name = "release_date") val releaseDate: String,
                         val title: String,
                         val video: Boolean,
                         @ColumnInfo(name = "vote_average") val voteAverage: Double,
                         @ColumnInfo(name = "vote_count") val voteCount: Int)