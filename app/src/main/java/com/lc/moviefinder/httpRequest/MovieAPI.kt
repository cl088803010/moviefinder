package com.lc.moviefinder.httpRequest

import com.lc.moviefinder.httpRequest.model.CastReponse
import com.lc.moviefinder.httpRequest.model.Movie
import com.lc.moviefinder.httpRequest.model.MovieResponse
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.core.Single
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query
import retrofit2.http.QueryMap

interface MovieAPI {

    @GET("/3/movie/upcoming")
    fun getUpcomingMovies(@QueryMap queries: Map<String, String>): Single<MovieResponse>

    @GET("/3/movie/top_rated")
    fun getTopRatedMovies(@QueryMap queries: Map<String, String>): Observable<MovieResponse>

    @GET("/3/movie/popular")
    fun getPopularMovies(@QueryMap queries: Map<String, String>): Observable<MovieResponse>

    @GET("/3/movie/now_playing")
    fun getCurrentShowingMovies(@QueryMap queries: Map<String, String>): Observable<MovieResponse>

    @GET("/3/movie/{movie_id}")
    fun getMovieByID(@Path("movie_id")movieID: Int, @QueryMap queries: Map<String, String>): Observable<Movie>

    @GET("/3/movie/{movie_id}}/credits")
    fun getCastsByMovieID(@Path("movie_id")movieID: Int, @Query("api_key") apiKey: String): Single<CastReponse>

}