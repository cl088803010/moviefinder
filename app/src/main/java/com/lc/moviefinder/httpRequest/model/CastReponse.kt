package com.lc.moviefinder.httpRequest.model

data class CastReponse(val id: Int, val cast: ArrayList<Cast>)