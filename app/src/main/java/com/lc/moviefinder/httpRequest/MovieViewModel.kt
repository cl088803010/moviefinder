package com.lc.moviefinder.httpRequest

import android.util.Log
import androidx.lifecycle.*
import com.lc.moviefinder.httpRequest.model.Cast
import com.lc.moviefinder.httpRequest.model.CastReponse
import com.lc.moviefinder.httpRequest.model.Movie
import com.lc.moviefinder.httpRequest.model.MovieResponse
import com.lc.moviefinder.room.WishListMovie
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.core.*
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.observers.DisposableCompletableObserver
import io.reactivex.rxjava3.observers.DisposableObserver
import io.reactivex.rxjava3.observers.DisposableSingleObserver
import io.reactivex.rxjava3.schedulers.Schedulers
import io.reactivex.rxjava3.subscribers.DisposableSubscriber
import kotlinx.coroutines.launch
import org.reactivestreams.Subscriber
import org.reactivestreams.Subscription
import java.lang.IllegalArgumentException

class MovieViewModel(private val repository: Repository): ViewModel(){

    private val mCompositeDisposable = CompositeDisposable()

    private val _upcomingMovies: MutableLiveData<List<Movie>> = MutableLiveData()
    val upcomingMovies: LiveData<List<Movie>> = _upcomingMovies

    private val _topRatedMovies: MutableLiveData<List<Movie>> = MutableLiveData()
    val topRatedMovies: LiveData<List<Movie>> = _topRatedMovies

    private val _popularMovies: MutableLiveData<List<Movie>> = MutableLiveData()
    val popularMovies: LiveData<List<Movie>> = _popularMovies

    private val _curShowingMovies: MutableLiveData<List<Movie>> = MutableLiveData()
    val curShowingMovies: LiveData<List<Movie>> = _curShowingMovies

    private val _movieDetail: MutableLiveData<Movie> = MutableLiveData()
    val movieDetail: LiveData<Movie> = _movieDetail

    private val _casts: MutableLiveData<List<Cast>> = MutableLiveData()
    val casts: LiveData<List<Cast>> = _casts

    private val _wishlistMovieInserted: MutableLiveData<Boolean> = MutableLiveData()
    val wishlistMovieInserted: LiveData<Boolean> = _wishlistMovieInserted

    private val _wishlistMovie: MutableLiveData<WishListMovie> = MutableLiveData()
    val wishlistMovie: LiveData<WishListMovie> = _wishlistMovie
    private val _wishlistMovieFound: MutableLiveData<Boolean> = MutableLiveData()
    val wishListMovieFound: LiveData<Boolean> = _wishlistMovieFound


    private val _wishlistMovieRemoved: MutableLiveData<Boolean> = MutableLiveData()
    val wishlistMovieRemoved: LiveData<Boolean> = _wishlistMovieRemoved

    private val _wishlistMovies: MutableLiveData<List<WishListMovie>> = MutableLiveData()
    val wishListMovies: LiveData<List<WishListMovie>> = _wishlistMovies


    fun getUpcomingMovies(queries: Map<String, String>){

        mCompositeDisposable.add(
            repository.getUpcomingMovies(queries)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(object : DisposableSingleObserver<MovieResponse>(){
                    override fun onSuccess(movieResponse : MovieResponse) {
                        _upcomingMovies.value = movieResponse.results
                    }

                    override fun onError(e: Throwable) {
                        e.printStackTrace()
                    }
                })
            )
    }

    fun getTopRatedMovies(queries: Map<String, String>) {


        repository.getTopRatedMovies(queries)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {
                    _topRatedMovies.value = it.results
                },
                {
                    it.printStackTrace()
                }
            )
    }

    fun getPopularMovies(queries: Map<String, String>) {
        repository.getPopularMovies(queries)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {
                    _popularMovies.value = it.results
                },
                {
                    it.printStackTrace()
                }
            )
    }

    fun getCurrentShowingMovies(queries: Map<String, String>) {
        repository.getCurrentShowingMovies(queries)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {
                    _curShowingMovies.value = it.results
                },
                {
                    it.printStackTrace()
                }
            )
    }

    fun getMovieByID(queries: Map<String, String>, movieID: Int){
        repository.getMovieByID(movieID, queries)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {
                    _movieDetail.value = it
                },
                {
                    it.printStackTrace()
                }
            )
    }

    fun getCastsByMovieID(movieID: Int, apiKey: String){
        mCompositeDisposable.add(
            repository.getCastsByMovieID(movieID, apiKey)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(object : DisposableSingleObserver<CastReponse>(){
                    override fun onSuccess(castReponse: CastReponse) {
                        _casts.value = castReponse.cast
                    }

                    override fun onError(e: Throwable) {
                        e.printStackTrace()
                    }

                }))
    }

    fun insertWishListMovie(movie: WishListMovie){
        mCompositeDisposable.add(
            repository.insertWishListMovie(movie)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(object : DisposableCompletableObserver(){
                override fun onComplete() {
                    _wishlistMovieInserted.value = true
                }

                override fun onError(e: Throwable) {
                    _wishlistMovieInserted.value = false
                }

            })
        )
    }

    fun getWishListMovies(){
        mCompositeDisposable.add(
            repository.getWishListMovies()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    {
                        _wishlistMovies.value = it
                    },
                    {
                        it.printStackTrace()
                    }
                )
        )
    }

    fun getWishListMovieByID(movieID: Int){
        mCompositeDisposable.add(
            repository.getWishListMovieByID(movieID)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    {
                        _wishlistMovie.value = it
                        _wishlistMovieFound.value = true
                    },{
                        _wishlistMovieFound.value = false
                    }
                )
        )



    }

    fun clearWishLisMovies(){
        mCompositeDisposable.add(
            repository.clearWishLisMovies()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(object : DisposableCompletableObserver(){
                    override fun onComplete() {
                        Log.i("MovieViewModel", "wishlist movies cleared")
                    }

                    override fun onError(e: Throwable) {
                       e.printStackTrace()
                    }

                })
        )
    }

    fun deleteWishListMovieByID(movieID: Int){
        mCompositeDisposable.add(
            repository.deleteWishListMovieByID(movieID)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(object : DisposableCompletableObserver(){
                    override fun onComplete() {
                        _wishlistMovieRemoved.value = true
                    }

                    override fun onError(e: Throwable) {
                        _wishlistMovieRemoved.value = false
                    }

                })
        )
    }


    override fun onCleared() {
        super.onCleared()
        mCompositeDisposable.dispose()
    }
}


class MovieViewModelFactory(val repository: Repository) : ViewModelProvider.Factory{
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if(modelClass.isAssignableFrom(MovieViewModel::class.java)){
            return MovieViewModel(repository) as T
        }

        throw IllegalArgumentException("Unknow view model")
    }

}