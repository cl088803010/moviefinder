package com.lc.moviefinder.httpRequest

import com.lc.moviefinder.httpRequest.model.CastReponse
import com.lc.moviefinder.httpRequest.model.Movie
import com.lc.moviefinder.httpRequest.model.MovieResponse
import com.lc.moviefinder.room.WishListMovie
import com.lc.moviefinder.room.WishListMovieDao
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.Flowable
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.core.Single
import kotlinx.coroutines.flow.Flow


class Repository(val movieAPI: MovieAPI, val wishListMovieDao: WishListMovieDao) {
    fun getUpcomingMovies(queries: Map<String, String>): Single<MovieResponse>{
        return movieAPI.getUpcomingMovies(queries)
    }

    fun getTopRatedMovies(queries: Map<String, String>): Observable<MovieResponse>{
        return movieAPI.getTopRatedMovies(queries)
    }

    fun getPopularMovies(queries: Map<String, String>): Observable<MovieResponse>{
        return movieAPI.getPopularMovies(queries)
    }

    fun getCurrentShowingMovies(queries: Map<String, String>): Observable<MovieResponse>{
        return movieAPI.getPopularMovies(queries)
    }

    fun getMovieByID(movieID: Int, queries: Map<String, String>): Observable<Movie>{
        return movieAPI.getMovieByID(movieID, queries)
    }

    fun getCastsByMovieID(movieID: Int, apiKey: String): Single<CastReponse>{
        return movieAPI.getCastsByMovieID(movieID, apiKey)
    }

    fun insertWishListMovie(movie: WishListMovie): Completable{
        return wishListMovieDao.insertWishListMovie(movie)
    }

    fun getWishListMovies(): Flowable<List<WishListMovie>>{
        return wishListMovieDao.getWishListMovies()
    }

    fun getWishListMovieByID(movieID: Int): Flowable<WishListMovie>{
        return wishListMovieDao.getWishListMovieByID(movieID)
    }

    fun clearWishLisMovies(): Completable{
        return wishListMovieDao.clearWishLisMovies()
    }

    fun deleteWishListMovieByID(movieID: Int): Completable{
        return wishListMovieDao.deleteWishListMovieByID(movieID)
    }
}