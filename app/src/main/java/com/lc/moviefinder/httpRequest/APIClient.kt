package com.lc.moviefinder.httpRequest

import com.lc.moviefinder.httpRequest.model.MovieResponse
import io.reactivex.rxjava3.core.Observable
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava3.RxJava3CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.QueryMap

class APIClient {

    private val retrofit: Retrofit
    val movieAPI: MovieAPI

    init {
        val httpLoggingInterceptor = HttpLoggingInterceptor()
        httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY)
        val okHttpClient = OkHttpClient().newBuilder().addInterceptor(httpLoggingInterceptor).build()
        retrofit = Retrofit
                    .Builder()
                    .client(okHttpClient)
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .addCallAdapterFactory(RxJava3CallAdapterFactory.create())
                    .build()
        movieAPI = retrofit.create(MovieAPI::class.java)
    }

    companion object{
        const val API_KEY = "756a621ecbb1176ee32055107c274bb7"
        const val BASE_URL = "https://api.themoviedb.org/"

        var mInstance: APIClient? = null

        fun getInstance(): APIClient = mInstance ?: APIClient()
    }
}