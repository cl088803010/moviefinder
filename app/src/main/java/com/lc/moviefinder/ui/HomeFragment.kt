package com.lc.moviefinder.ui

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.viewModels
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.lc.moviefinder.Constants
import com.lc.moviefinder.R
import com.lc.moviefinder.adapter.HomeRVAdapter
import com.lc.moviefinder.adapter.HomeViewPagerAdapter
import com.lc.moviefinder.databinding.FragmentHomeBinding
import com.lc.moviefinder.httpRequest.APIClient
import com.lc.moviefinder.httpRequest.MovieViewModel
import com.lc.moviefinder.httpRequest.MovieViewModelFactory
import com.lc.moviefinder.httpRequest.Repository
import com.lc.moviefinder.httpRequest.model.Movie
import com.lc.moviefinder.room.MovieDatabase
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class HomeFragment : Fragment() {

    val movieViewModel: MovieViewModel by viewModels{
        MovieViewModelFactory(Repository(APIClient.getInstance().movieAPI, MovieDatabase.getInstance(requireContext().applicationContext).WishListMovieDao()))
    }
    private lateinit var bd: FragmentHomeBinding
    //private val popularAdapter = HomeRVAdapter(ArrayList())
    @Inject lateinit var popularAdapter: HomeRVAdapter
    private val upcomingAdapter = HomeRVAdapter(ArrayList())
    private val topRatedAdapter = HomeRVAdapter(ArrayList())
    private val curShowingAdapter = HomeViewPagerAdapter(ArrayList())
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        bd =  FragmentHomeBinding.inflate(inflater, container, false)
        return bd.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupUI()
        getData()
        addDataObserve()
    }

    private fun setupUI(){
        bd.popularLayout.titleTv.setText("Popular")
        bd.topRatedLayout.titleTv.setText("Top rated")
        bd.upcomingLayout.titleTv.setText("Upcoming")
        bd.curShowingLayout.titleTv.setText("Currently showing")

        setupRVs()
        setupViewPager()
        setupClickListener()
    }

    private fun setupViewPager(){
        bd.curShowingLayout.movieViewPager.adapter = curShowingAdapter
    }

    private fun setupRVs(){
        bd.popularLayout.movieRv.adapter = popularAdapter
        bd.popularLayout.movieRv.layoutManager = LinearLayoutManager(context, RecyclerView.HORIZONTAL, false)

        bd.upcomingLayout.movieRv.adapter = upcomingAdapter
        bd.upcomingLayout.movieRv.layoutManager = LinearLayoutManager(context, RecyclerView.HORIZONTAL, false)

        bd.topRatedLayout.movieRv.adapter = topRatedAdapter
        bd.topRatedLayout.movieRv.layoutManager = LinearLayoutManager(context, RecyclerView.HORIZONTAL, false)
    }

    private fun getData(){
        val queryMap = HashMap<String, String>()
        queryMap.put("api_key", APIClient.API_KEY)
        queryMap.put("page", "1")

        movieViewModel.getPopularMovies(queryMap)
        movieViewModel.getUpcomingMovies(queryMap)
        movieViewModel.getTopRatedMovies(queryMap)
        movieViewModel.getCurrentShowingMovies(queryMap)
    }

    private fun addDataObserve(){
        movieViewModel.popularMovies.observe(viewLifecycleOwner){
            popularAdapter.addMovies(it as ArrayList<Movie>)
        }

        movieViewModel.topRatedMovies.observe(viewLifecycleOwner){
            topRatedAdapter.addMovies(it as ArrayList<Movie>)
        }

        movieViewModel.upcomingMovies.observe(viewLifecycleOwner){
            upcomingAdapter.addMovies(it as ArrayList<Movie>)
        }

        movieViewModel.curShowingMovies.observe(viewLifecycleOwner){
            curShowingAdapter.addMovies(it as ArrayList<Movie>)
        }
    }

    private fun setupClickListener(){
        bd.curShowingLayout.viewAllTv.setOnClickListener {
            val bundle = bundleOf(Pair(MOVIE_CATEGORY_KEY, Constants.MovieCategory.CURRENTLY_SHOWING))
            Navigation.findNavController(it).navigate(R.id.action_homeFragment_to_moviesFragment, bundle)
        }

        bd.upcomingLayout.viewAllTv.setOnClickListener{
            val bundle = bundleOf(MOVIE_CATEGORY_KEY to Constants.MovieCategory.UPCOMING)
            Navigation.findNavController(it).navigate(R.id.action_homeFragment_to_moviesFragment, bundle)
        }

        bd.topRatedLayout.viewAllTv.setOnClickListener{
            val bundle = bundleOf(MOVIE_CATEGORY_KEY to Constants.MovieCategory.TOP_RATED)
            Navigation.findNavController(it).navigate(R.id.action_homeFragment_to_moviesFragment, bundle)
        }

        bd.popularLayout.viewAllTv.setOnClickListener{
            val bundle = bundleOf(MOVIE_CATEGORY_KEY to Constants.MovieCategory.POPULAR)
            Navigation.findNavController(it).navigate(R.id.action_homeFragment_to_moviesFragment, bundle)
        }
    }

    companion object {
        @JvmStatic
        fun newInstance() = HomeFragment()
        const val MOVIE_CATEGORY_KEY = "movieCategory"

    }
}