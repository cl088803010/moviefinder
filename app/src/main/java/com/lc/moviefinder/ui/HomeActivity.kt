package com.lc.moviefinder.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import com.lc.moviefinder.R
import com.lc.moviefinder.databinding.ActivityMainBinding
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class HomeActivity : AppCompatActivity() {
    lateinit var bd: ActivityMainBinding
    lateinit var navController: NavController

    @Inject lateinit var helloWorldStr: String
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        bd = ActivityMainBinding.inflate(layoutInflater)
        setContentView(bd.root)
        setupNacController()
        setupBtmNavSelectListener()

    }

    private fun setupNacController(){
        val navHostFragment = supportFragmentManager.findFragmentById(R.id.nav_host_fragment) as NavHostFragment
        navController = navHostFragment.navController
    }

    private fun setupBtmNavSelectListener(){

        bd.btmNavView.setOnItemSelectedListener {
            when(it.itemId){
                R.id.home -> {
                    navController.navigate(R.id.homeFragment)
                    return@setOnItemSelectedListener true
                }

                R.id.wishList -> {
                    navController.navigate(R.id.wishlistMoviesFragment)
                    return@setOnItemSelectedListener true
                }

                R.id.account -> {
                    return@setOnItemSelectedListener true
                }

                else -> {
                    return@setOnItemSelectedListener false
                }
            }
        }

        bd.btmNavView.setOnItemReselectedListener {
            return@setOnItemReselectedListener
        }
    }

}