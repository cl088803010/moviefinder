package com.lc.moviefinder.ui

import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import androidx.activity.addCallback
import androidx.fragment.app.viewModels
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.lc.moviefinder.Constants
import com.lc.moviefinder.R
import com.lc.moviefinder.adapter.CastsRVAdapter
import com.lc.moviefinder.adapter.HomeRVAdapter
import com.lc.moviefinder.databinding.FragmentMovieDetailBinding
import com.lc.moviefinder.httpRequest.APIClient
import com.lc.moviefinder.httpRequest.MovieViewModel
import com.lc.moviefinder.httpRequest.MovieViewModelFactory
import com.lc.moviefinder.httpRequest.Repository
import com.lc.moviefinder.httpRequest.model.Cast
import com.lc.moviefinder.httpRequest.model.Movie
import com.lc.moviefinder.room.MovieDatabase
import com.lc.moviefinder.room.WishListMovie

class MovieDetailFragment : Fragment() {

    private lateinit var bd: FragmentMovieDetailBinding
    private val movieViewModel: MovieViewModel by viewModels{
        MovieViewModelFactory(Repository(APIClient.getInstance().movieAPI, MovieDatabase.getInstance(requireContext().applicationContext).WishListMovieDao()))
    }
    private var movieId = 0
    private val adapter = CastsRVAdapter(arrayListOf())
    private var isFound: Boolean = false
    private var mMovie: Movie? = null
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        bd = FragmentMovieDetailBinding.inflate(inflater, container, false)
        return bd.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        arguments?.let {
            movieId = it.getInt(HomeRVAdapter.ViewHolder.MOVIE_ID_KEY)
        }

        bd.movieIv.clipToOutline = true
        setupCastsRv()
        getData()
        observeData()
        setupClickListener()
    }



    private fun getData(){
        val queryMap = HashMap<String, String>()
        queryMap.put("api_key", APIClient.API_KEY)
        movieViewModel.getMovieByID(queryMap, movieId)
        movieViewModel.getCastsByMovieID(movieId, APIClient.API_KEY)
        movieViewModel.getWishListMovieByID(movieId)

    }

    private fun observeData(){
        movieViewModel.movieDetail.observe(viewLifecycleOwner){
            mMovie = it
            updateUI(it)
        }

        movieViewModel.casts.observe(viewLifecycleOwner){
            adapter.addCasts(it as ArrayList<Cast>)
        }

        movieViewModel.wishListMovieFound.observe(viewLifecycleOwner){
            updateWishListIc(it)
            isFound = it
        }

        movieViewModel.wishlistMovieInserted.observe(viewLifecycleOwner){
            val toast: String
            toast = if(it) "inserted" else "insert failed"
            Toast.makeText(context, "Movie ${toast}", Toast.LENGTH_SHORT).show()
        }

        movieViewModel.wishlistMovieRemoved.observe(viewLifecycleOwner){
            val toast: String
            toast = if(it) "removed" else "removal failed"
            Toast.makeText(context, "Movie ${toast}", Toast.LENGTH_SHORT).show()
            updateWishListIc(false)
            isFound = false
        }
    }

    private fun setupCastsRv(){
        bd.castRv.adapter =adapter
        bd.castRv.layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
    }

    private fun updateUI(movie: Movie){
        bd.titleTv.setText(movie.title)
        bd.contentTv.setText(movie.overview)
        bd.genreTv.setText(movie.releaseDate)
        context?.let {
            Glide.with(it).load(Constants.IMAGE_BASE_URL + movie.posterPath).into(bd.movieIv)
        }
    }

    private fun updateWishListIc(isFound: Boolean){
        if (isFound){
            bd.wishListIcIv.setImageResource(R.drawable.ic_baseline_playlist_added)
        }else{
            bd.wishListIcIv.setImageResource(R.drawable.ic_playlist_add)
        }
    }

    private fun setupClickListener(){
        bd.wishListIcIv.setOnClickListener {
            if(!isFound){
                mMovie?.let {
                    val wishListMovie = WishListMovie(it.adult,
                        it.backdropPath, it.id, it.originalLanguage,
                        it.originalTitle, it.overview, it.popularity,
                        it.posterPath, it.releaseDate, it.originalTitle,
                        it.video, it.voteAverage, it.voteCount
                    )
                    movieViewModel.insertWishListMovie(wishListMovie)
                }
            }else{
                movieViewModel.deleteWishListMovieByID(movieId)
            }
        }
    }

    companion object {

        @JvmStatic
        fun newInstance() =
            MovieDetailFragment()
    }
}