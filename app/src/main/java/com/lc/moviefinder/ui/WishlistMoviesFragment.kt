package com.lc.moviefinder.ui

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.GridLayoutManager
import com.lc.moviefinder.R
import com.lc.moviefinder.adapter.HomeRVAdapter
import com.lc.moviefinder.adapter.MoviesRVAdapter
import com.lc.moviefinder.databinding.FragmentWishlistMoviesBinding
import com.lc.moviefinder.httpRequest.APIClient
import com.lc.moviefinder.httpRequest.MovieViewModel
import com.lc.moviefinder.httpRequest.MovieViewModelFactory
import com.lc.moviefinder.httpRequest.Repository
import com.lc.moviefinder.httpRequest.model.Movie
import com.lc.moviefinder.room.MovieDatabase
import com.lc.moviefinder.room.WishListMovie

class WishlistMoviesFragment : Fragment() {

    lateinit var bd: FragmentWishlistMoviesBinding
    val adapter = HomeRVAdapter(arrayListOf())
    val movieViewModel : MovieViewModel by viewModels{
        MovieViewModelFactory(Repository(APIClient.getInstance().movieAPI, MovieDatabase.getInstance(requireContext()).WishListMovieDao()))
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        bd = FragmentWishlistMoviesBinding.inflate(inflater, container, false)
        return bd.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupMoviesRv()
        getData()
        observeData()
    }

    private fun setupMoviesRv(){
        bd.moviesRv.adapter = adapter
        bd.moviesRv.layoutManager = GridLayoutManager(context, 2)
    }

    private fun getData(){
        movieViewModel.getWishListMovies()
    }

    private fun observeData(){
        movieViewModel.wishListMovies.observe(viewLifecycleOwner){
            val movies = arrayListOf<Movie>()
            it.forEach {
                val movie = Movie(
                    it.adult, it.backdropPath, listOf(),
                    it.id, it.originalLanguage, it.originalTitle,
                    it.overview, it.popularity, it.posterPath,
                    it.releaseDate, it.originalTitle, it.video,
                    it.voteAverage, it.voteCount)
                movies.add(movie)
            }
            adapter.addMovies(movies)
        }
    }
}