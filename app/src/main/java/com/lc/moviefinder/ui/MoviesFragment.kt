package com.lc.moviefinder.ui

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.GridLayoutManager
import com.bumptech.glide.load.model.MultiModelLoaderFactory
import com.lc.moviefinder.Constants
import com.lc.moviefinder.R
import com.lc.moviefinder.adapter.MoviesRVAdapter
import com.lc.moviefinder.databinding.FragmentMoviesBinding
import com.lc.moviefinder.httpRequest.APIClient
import com.lc.moviefinder.httpRequest.MovieViewModel
import com.lc.moviefinder.httpRequest.MovieViewModelFactory
import com.lc.moviefinder.httpRequest.Repository
import com.lc.moviefinder.httpRequest.model.Movie
import com.lc.moviefinder.room.MovieDatabase

class MoviesFragment : Fragment() {

    private lateinit var bd: FragmentMoviesBinding
    private val movieViewModel: MovieViewModel by viewModels {
        MovieViewModelFactory(Repository( APIClient.getInstance().movieAPI, MovieDatabase.getInstance(requireContext().applicationContext).WishListMovieDao()))
    }
    private var movieCategory: Constants.MovieCategory = Constants.MovieCategory.POPULAR
    private val adapter = MoviesRVAdapter(arrayListOf())

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        bd = FragmentMoviesBinding.inflate(inflater, container, false)
        return bd.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupMovieCategory()
        getData()
        setupUI()
        observeData()
    }

    override fun onDestroy() {
        super.onDestroy()
    }

    private fun setupMovieCategory(){
        movieCategory = arguments?.getSerializable("movieCategory") as Constants.MovieCategory
    }

    private fun setupUI(){
        setupTitle()
        setupRecyclerView()
    }

    private fun setupTitle(){
        var title: String
        when(movieCategory){
            Constants.MovieCategory.POPULAR -> title = "Popular"
            Constants.MovieCategory.CURRENTLY_SHOWING -> title = "Currently showing"
            Constants.MovieCategory.UPCOMING -> title = "Upcoming"
            Constants.MovieCategory.TOP_RATED -> title = "Top rated"
        }
        bd.titleTv.setText(title)
        Log.i("MoviesFragment", "title is ${title}")
    }


    private fun setupRecyclerView(){
        bd.moviesRv.adapter = adapter
        bd.moviesRv.layoutManager = GridLayoutManager(context, 2)
    }

    private fun getData(){
        val queryMap = HashMap<String, String>()
        queryMap.put("api_key", APIClient.API_KEY)
        queryMap.put("page", "1")

        when(movieCategory){
            Constants.MovieCategory.POPULAR -> movieViewModel.getPopularMovies(queryMap)
            Constants.MovieCategory.CURRENTLY_SHOWING -> movieViewModel.getCurrentShowingMovies(queryMap)
            Constants.MovieCategory.UPCOMING -> movieViewModel.getUpcomingMovies(queryMap)
            Constants.MovieCategory.TOP_RATED -> movieViewModel.getTopRatedMovies(queryMap)
        }
    }

    private fun observeData(){

        movieViewModel.curShowingMovies.observe(viewLifecycleOwner){
            adapter.addMovies(it as ArrayList<Movie>)
        }
        movieViewModel.popularMovies.observe(viewLifecycleOwner){
            adapter.addMovies(it as ArrayList<Movie>)
        }
        movieViewModel.upcomingMovies.observe(viewLifecycleOwner){
            adapter.addMovies(it as ArrayList<Movie>)
        }
        movieViewModel.topRatedMovies.observe(viewLifecycleOwner){
            adapter.addMovies(it as ArrayList<Movie>)
        }
    }

    companion object {
        @JvmStatic
        fun newInstance() =
            MoviesFragment()
    }
}